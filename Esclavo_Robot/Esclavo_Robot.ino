// Pines
int valorx;
int valory;
int valmediomax = 532;
int valmediomin = 492;
int vel1;
int vel2;
int enable_d = 11;
int motor_d1 = 12;
int motor_d2 = 13;
int enable_i = 10;
int motor_i1 = 9;
int motor_i2 = 8;

void setup(){
  pinMode(enable_d,OUTPUT);
  pinMode(motor_d1,OUTPUT);
  pinMode(motor_d2,OUTPUT);
  pinMode(enable_i,OUTPUT);
  pinMode(motor_i1,OUTPUT);
  pinMode(motor_i2,OUTPUT); 
  Serial.begin(9600);
}
 
void loop(){
  valorx = analogRead(A0);
  valory = analogRead(A1);
  if(valory > valmediomax) {
    if(valorx>valmediomax){
      vel1 = map(valorx, 532, 1023, 0, 180);
      derecha_b(vel1);
    }
    else if(valorx < valmediomin){
      vel2 = map(valorx, 492, 0, 0, 180);
      izquierda_b(vel2);
    }
    else{
      vel1 = map(valory, 532, 1023, 0, 255);
      vel2 = map(valory, 532, 1023, 0, 255);
      atras(vel1,vel2);
    }
  }
  else if(valory < valmediomin){
    if(valorx>valmediomax){
      vel1 = map(valorx, 532, 1023, 0, 180);
      derecha_a(vel1);
    }
    else if(valorx < valmediomin){
      vel2 = map(valorx, 492, 0, 0, 180);
      izquierda_a(vel2);
    }
    else{
      vel1 = map(valory, 492, 0, 0, 255);
      vel2 = map(valory, 492, 0, 0, 255);
      adelante(vel1,vel2);
    }
  }
  else{
    if(valorx > valmediomax){
      vel2 = map(valorx, 532, 1023, 0, 255);
      derecha(vel2);
    }
    else if(valorx < valmediomin){
      vel1 = map(valorx, 532, 1023, 0, 255);
      izquierda(vel1);
    }
    else{
      pare();
    }
  }
}
// Funciones
void adelante( int a, int b){
    digitalWrite(motor_d1, LOW);
    digitalWrite(motor_d2, HIGH);
    digitalWrite(motor_i1, LOW);
    digitalWrite(motor_i2, HIGH);
    analogWrite(enable_d, a);
    analogWrite(enable_i, b);
    Serial.println("a");
}
void derecha_a( int a){
    digitalWrite(motor_d1, LOW);
    digitalWrite(motor_d2, HIGH);
    digitalWrite(motor_i1, LOW);
    digitalWrite(motor_i2, HIGH);
    analogWrite(enable_d, a);
    analogWrite(enable_i, 255);
    Serial.println("b");
}
void izquierda_a( int a){
    digitalWrite(motor_d1, LOW);
    digitalWrite(motor_d2, HIGH);
    digitalWrite(motor_i1, LOW);
    digitalWrite(motor_i2, HIGH);
    analogWrite(enable_d, 255);
    analogWrite(enable_i, a);
    Serial.println("c");
}
void derecha_b( int a){
    digitalWrite(motor_d1, HIGH);
    digitalWrite(motor_d2, LOW);
    digitalWrite(motor_i1, HIGH);
    digitalWrite(motor_i2, LOW);
    analogWrite(enable_d, a);
    analogWrite(enable_i, 255);
    Serial.println("d");
}
void izquierda_b( int a){
    digitalWrite(motor_d1, HIGH);
    digitalWrite(motor_d2, LOW);
    digitalWrite(motor_i1, HIGH);
    digitalWrite(motor_i2, LOW);
    analogWrite(enable_d, 255);
    analogWrite(enable_i, a);
    Serial.println("e");
}
void atras( int a, int b){
    digitalWrite(motor_d1, HIGH);
    digitalWrite(motor_d2, LOW);
    digitalWrite(motor_i1, HIGH);
    digitalWrite(motor_i2, LOW);
    analogWrite(enable_d, a);
    analogWrite(enable_i, b);
    Serial.println("f");
}
void derecha(int a){
  digitalWrite(motor_i1, LOW);
  digitalWrite(motor_i2, HIGH);
  analogWrite(enable_d,0);
  analogWrite(enable_i,a);
  Serial.println("g");
}
void izquierda(int a){
  digitalWrite(motor_d1, LOW);
  digitalWrite(motor_d2, HIGH);
  analogWrite(enable_d,a);
  analogWrite(enable_i,0);
  Serial.println("h");
}
void pare(){
  analogWrite(enable_d,0);
  analogWrite(enable_i,0);
  Serial.println("i");
}
