// Pines
int vel2;
int vel1;
int num;
int i=1;
int a;
int enable_d = 11;
int motor_d1 = 12;
int motor_d2 = 13;
int enable_i = 10;
int motor_i1 = 9;
int motor_i2 = 8;
void setup(){
  pinMode(enable_d,OUTPUT);
  pinMode(motor_d1,OUTPUT);
  pinMode(motor_d2,OUTPUT);
  pinMode(enable_i,OUTPUT);
  pinMode(motor_i1,OUTPUT);
  pinMode(motor_i2,OUTPUT); 
  Serial.begin(9600);
}
 
void loop(){
  delay(100);
  if(Serial.available() > 0 && i==1)
  {
    vel1=Serial.read();
    Serial.print("El valor 1 = ");
    Serial.println(vel1);
    a=2;
    Serial.flush();
  }
  delay(100);
  if(Serial.available() > 0 && i==2)
  {
    vel2=Serial.read();
    Serial.print("El valor 2 = ");
    Serial.println(vel2);
    a=3;
    Serial.flush();
  }
  if(Serial.available() > 0 && i==3)
  {
    num=Serial.read();
    Serial.print("El num = ");
    Serial.println(num);
    a=1;
    Serial.flush();
  }
  delay(100);
  if(a==3){
    i=3;
  }
  if(a==2){
    i=2;
  }
  if(a==1){
    i=1;
  }
  switch (num){
  case 1:
  derecha_b(vel1);
  break;
  case 2:
  izquierda_b(vel2);
  break;
  case 3:
  atras(vel1,vel2);
  break;
  case 4:
  derecha_a(vel1);
  break;
  case 5:
  izquierda_a(vel2);
  break;
  case 6:
  adelante(vel1,vel2);
  break;
  case 7:
  derecha(vel2);
  break;
  case 8:
  izquierda(vel1);
  break;
  case 9:
  pare();
  break;}
}


void adelante( int a, int b){
    digitalWrite(motor_d1, LOW);
    digitalWrite(motor_d2, HIGH);
    digitalWrite(motor_i1, LOW);
    digitalWrite(motor_i2, HIGH);
    analogWrite(enable_d, a);
    analogWrite(enable_i, b);

}
void derecha_a( int a){
    digitalWrite(motor_d1, LOW);
    digitalWrite(motor_d2, HIGH);
    digitalWrite(motor_i1, LOW);
    digitalWrite(motor_i2, HIGH);
    analogWrite(enable_d, a);
    analogWrite(enable_i, 255);

}
void izquierda_a( int a){
    digitalWrite(motor_d1, LOW);
    digitalWrite(motor_d2, HIGH);
    digitalWrite(motor_i1, LOW);
    digitalWrite(motor_i2, HIGH);
    analogWrite(enable_d, 255);
    analogWrite(enable_i, a);

}
void derecha_b( int a){
    digitalWrite(motor_d1, HIGH);
    digitalWrite(motor_d2, LOW);
    digitalWrite(motor_i1, HIGH);
    digitalWrite(motor_i2, LOW);
    analogWrite(enable_d, a);
    analogWrite(enable_i, 255);

}
void izquierda_b( int a){
    digitalWrite(motor_d1, HIGH);
    digitalWrite(motor_d2, LOW);
    digitalWrite(motor_i1, HIGH);
    digitalWrite(motor_i2, LOW);
    analogWrite(enable_d, 255);
    analogWrite(enable_i, a);
  
}
void atras( int a, int b){
    digitalWrite(motor_d1, HIGH);
    digitalWrite(motor_d2, LOW);
    digitalWrite(motor_i1, HIGH);
    digitalWrite(motor_i2, LOW);
    analogWrite(enable_d, a);
    analogWrite(enable_i, b);

}
void derecha(int a){
  digitalWrite(motor_i1, LOW);
  digitalWrite(motor_i2, HIGH);
  analogWrite(enable_d,0);
  analogWrite(enable_i,a);

}
void izquierda(int a){
  digitalWrite(motor_d1, LOW);
  digitalWrite(motor_d2, HIGH);
  analogWrite(enable_d,a);
  analogWrite(enable_i,0);

}
void pare(){
  analogWrite(enable_d,0);
  analogWrite(enable_i,0);
}
