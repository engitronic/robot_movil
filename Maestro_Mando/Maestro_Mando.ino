// Pines
int valorx;
int valory;
int valmediomax = 532;
int valmediomin = 492;
char vel1;
char vel2;
char num;

void setup(){
  Serial.begin(9600);
}
 
void loop(){
  valorx = analogRead(A0);
  valory = analogRead(A1);
  if(valory > valmediomax) {
    if(valorx>valmediomax){
      vel1 = map(valorx, 522, 1023, 0, 180);
      vel2=255;
      num = 1;
    }
    else if(valorx < valmediomin){
      vel2 = map(valorx, 492, 0, 0, 180);
      vel1=255;
      num = 2;
    }
    else{
      vel1 = map(valory, 532, 1023, 0, 255);
      vel2 = map(valory, 532, 1023, 0, 255);
      num = 3;
    }
  }
  else if(valory < valmediomin){
    if(valorx>valmediomax){
      vel1 = map(valorx, 532, 1023, 0, 180);
      vel2=255;
      num = 4;
    }
    else if(valorx < valmediomin){
      vel2 = map(valorx, 492, 0, 0, 180);
      vel1=255;
      num = 5;
    }
    else{
      vel1 = map(valory, 492, 0, 0, 255);
      vel2 = map(valory, 492, 0, 0, 255);
      num = 6;
    }
  }
  else{
    if(valorx > valmediomax){
      vel2 = map(valorx, 532, 1023, 0, 255);
      vel1=0;
      num = 7;
    }
    else if(valorx < valmediomin){
      vel1 = map(valorx, 492, 0, 0, 255);
      vel2 = 0;
      num = 8;
    }
    else{
      vel1=0;
      vel2=0;
      num = 9;
    }
  }
  Serial.print(vel1);
  delay(300);
  Serial.print(vel2);
  delay(300);
  Serial.print(num);
  delay(300);
//  Serial.println(valorx);
//  Serial.println(valory);
}

