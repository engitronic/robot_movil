#include <SoftwareSerial.h>   // Librería  SoftwareSerial  
SoftwareSerial Bluetooth(10,11);     // Pines RX y TX del Arduino conectados al Bluetooth
 
void setup()
{
  Bluetooth.begin(9600);  // Inicializamos el puerto serie Bluetooth 
  Serial.begin(9600);     // Inicializamos  el puerto serie  
}
 
void loop(){
  if(Bluetooth.available()) {    // Si llega un dato por el puerto Bluetooth se envía al monitor serial
    Serial.write(Bluetooth.read());
  }
  if(Serial.available()) {       // Si llega un dato por el monitor serial se envía al puerto Bluetooth
     Bluetooth.write(Serial.read());
  }
}
